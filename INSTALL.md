### Installation

## Prerequisites

For the Laravel framework you need:

* PHP >= 7.2.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* BCMath PHP Extension

You will also need the **LDAP PHP Extension** and the relevant PHP extension for the type of database that you plan to use. The following instructions assume that you will be using SQLite.

Since Laravel utilizes [Composer](https://getcomposer.org) for its dependencies, make sure you have Composer installed.


## Deployment

1. Inside the project directory, execute:

        composer install

2. After the installation of all the necessary dependencies is complete, copy the file `.env.universis` to `.env`:

        cp .env.universis .env

3. Create an empty database file:

        touch database/database.sqlite

4. Initialize the database and create the necessary keys for Laravel and Laravel Passport:

        php artisan migrate
        php artisan key:generate
        php artisan passport:keys

5. Create a client:

        php artisan passport:client

    Notes:
    - When asked to assign this client to a user ID just press ENTER.
    - You can name the client anything you want.
    - When entering the callback URL you **must** append the query delimiter (?) at the end. See the following example.

    ```
    Which user ID should the client be assigned to?:
    >

    What should we name the client?:
    > universis-student

    Where should we redirect the request after authorization?[http://localhost/auth/callback]:
    > http://localhost:7001/#/auth/callback?

    New client created successfully.
    Client ID: 1
    Client secret: …
    ```

    You will need the Client ID when configuring your application (The Client secret will not be needed for the grant type that we will be using).

## Development server

To try out the server, execute:

    php artisan serve

The server will be accessible at `http://localhost:8000/` .

To register a new user, navigate to `http://localhost:8000/register` . The email you input for the new user **must** be one of an existing account at the UniverSIS database.