<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Τα στοιχεία που εισάγατε δεν είναι σωστά.',
    'throttle' => 'Έγιναν πολλές λανθασμένες προσπάθειες. Παρακαλώ δοκιμάστε ξανά σε :seconds δευτερόλεπτα.',
    'footer' => 'Διεύθυνση Μηχανοργάνωσης - ΔΠΘ',
    'title' => '**e-γραμματεία**',
];
