@inject('file', 'Illuminate\Support\Facades\File')
@php ($curLocale = app()->getLocale())

<!DOCTYPE html>
<html lang="{{ $curLocale }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="SIS Authentication Services">
    <meta name="author" content="">
    <meta name="keyword" content="SIS Authentication Services">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

   {{-- <!-- Icons -->
   <link href="/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   <link href="/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="oval app">
    <nav class="navbar navbar-top fixed-top navbar-expand-sm">
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">

                @foreach (config('languages') as $lang => $language)
                    @if ($lang != $curLocale)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                        </li>
                    @endif
                @endforeach
    
            </ul>
        </div>
    </nav>
    <main class="container-fluid">
        <div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="mt-7">

                        @yield('content')

                    </div>
                </div>
                <div class="col-lg-6 d-none d-xl-block d-lg-block">
                    <div class="row justify-content-center mt-7 text-white">
                        <div class="col-8 col-sm-6">
                            <div class="doc-block">
                                @parsedown($file::get(resource_path('markdown/login.' . $curLocale . '.md')))
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-sm">
                <a class="navbar-brand" href="#">{{ __('auth.footer') }}</a>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="#" title="{{ __('Available soon') }}">{{ __('Support') }}</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </footer>
</body>
</html>
