@extends('layouts.app')
@section('content')

<div class="row justify-content-center">
    <div class="col-md-9">
        <div class="card-group">
            <div class="card border-0">
                <div class="card-body">
                    <form class="form" name="form" method="POST" action="{{ route('logout') }}">
                        <h1 class="text-primary text-center card-title">@parsedown(__('auth.title'))</h1>
                        @csrf
                        <div class="text-center mb-5 mt-5">
                            <img src="svg/user_icon.svg" alt="User icon">
                        </div>
                        <div class="mb-5">
                            <h3 class="text-primary text-center card-title">{{ __('You are logged in') }}!</h3>
                            <h6 class="text-primary text-center">{{ __('Username') }}: {{ Auth::user()->username }}</h6>
                        </div>
                        <div class="text-center pt-4">
                            <button type="submit" class="btn btn-success px-4 text-uppercase">{{ __('Logout') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
