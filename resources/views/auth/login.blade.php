@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-9">
        <div class="card-group">
            <div class="card border-0">
                <div class="card-body">
                    <form class="form" name="form" method="POST" action="{{ route('login') }}">
                        <h1 class="text-primary text-center card-title">@parsedown(__('auth.title'))</h1>
                        @csrf
                        <div class="text-center mb-5 mt-5">
                            <img src="svg/user_icon.svg" alt="User icon">
                        </div>
                        <div class="mb-4">
                            <label class="text-uppercase" for="username">{{ __('Username') }}</label>
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
                                name="username" value="{{ old('username') }}" required autofocus>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="mb-4">
                            <label class="text-uppercase" for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                name="password" required>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="text-center pt-4">
                            <button type="submit" class="btn btn-success px-4 text-uppercase">{{ __('Login') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection