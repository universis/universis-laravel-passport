### OAuth2 server


## Installation
  Installation instructions can be found in the [installation file](INSTALL.md).

## Authentication using LDAP

The [Adldap2-Laravel](https://github.com/Adldap2/Adldap2-Laravel) package is used for LDAP authentication. To configure it, set the relevant `LDAP_*` options in the file `.env`. For more options, check the `config/ldap.php` and `config/ldap_auth.php` files.

To enable authentication via LDAP, edit the `config/auth.php` file and set your user provider to use the `ldap` driver:

```php
'providers' => [
        'users' => [
            'driver' => 'ldap', // Was 'eloquent'.
            'model' => App\User::class,
        ],
```

Note that once you enable the `ldap` driver, any users in your database that do not exist in the LDAP directory will not be able to login.

## Editing the CSS

While you could manually edit the CSS files in the `public/css` directory, it is recommended to use the Sass file infrastructure in the `resources/sass` directory. To compile your Sass files, you need to have `npm` installed. Install the project's dependencies, by running inside your project root:

```
npm install
```

The Sass files can be compiled to CSS with Laravel Mix, using the instructions found in the `webpack.mix.js` file. The default instructions will compile the `resources/sass/app.scss` file and place the compiled CSS in the `public/css` directory. To do that, run:

```
npm run dev
```

or, if you want the minified version:

```
npm run production
```