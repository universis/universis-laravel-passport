#!/bin/bash

php -r "copy('https://composer.github.io/installer.sig', 'composer.sig');"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
actualSig="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$(cat composer.sig)" != "${actualSig}" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php composer.sig
    exit 1
fi

php composer-setup.php --quiet
result=$?
rm composer-setup.php composer.sig
exit ${result}
