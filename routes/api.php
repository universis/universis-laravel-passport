<?php

use Illuminate\Http\Request;
use App\User;
use App\AccessToken;
use Laravel\Passport\Passport;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/tokeninfo', function (Request $request) {
    // get token data from jwt
    $token = (new Parser())->parse((string) $request->access_token);
    $data = new ValidationData(); // use the current time to validate (iat, nbf and exp)
    $publicKey = new Key('file://'.Passport::keyPath('oauth-public.key'));
    $accessToken = AccessToken::where('id', $token->getClaim('jti'))->first();
    $validSignature = $token->verify(new Sha256(), $publicKey);
    $active = ($validSignature &&
                $accessToken &&
                !$accessToken->revoked &&
                $token->validate($data)) ? true : false;
    if ($active) {
        $user = User::where('id', $token->getClaim('sub'))->first();
        return response(['active' => $active,
                        'scope' => $token->getClaim('scopes')[0],
                        'client_id' => $token->getClaim('aud'),
                        'username' => $user->username,
                        'exp' => $token->getClaim('exp')]);
    } else {
        return response(['active' => false]);
    }
})->middleware('checkip');

Route::get('/logout', function (Request $request) {
    // get token data from jwt
    $token = (new Parser())->parse((string) $request->bearerToken());
    $user = $request->user();
    $user->tokens->find($token->getClaim('jti'))->revoke();
    return ['status' => 'revoked'];
})->middleware('auth:api');