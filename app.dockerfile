FROM php:7.3-fpm-buster

WORKDIR /var/www
ENV TZ=Europe/Athens

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    apt-get update && \
    apt-get install -y libldap2-dev unzip && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install pdo_mysql ldap

COPY . /var/www

RUN /bin/bash composer-install.sh && \
    COMPOSER_ALLOW_SUPERUSER=1 php composer.phar install --optimize-autoloader --no-dev && \
    rm composer-install.sh composer.phar

RUN mv .env.universis .env && \
    php artisan key:generate && \
    php artisan passport:keys && \
    chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
