FROM nginx:1.17

ARG domain=example.com
ENV TZ=Europe/Athens

COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY public /var/www/public

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    sed -i "s:example.com:$domain:g" /etc/nginx/conf.d/default.conf
