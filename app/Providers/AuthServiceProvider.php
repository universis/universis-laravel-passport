<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::enableImplicitGrant();
        Passport::tokensExpireIn(now()->addHours(2));
        Passport::tokensCan([
            'students' => 'Access your data for the student interface',
            'teachers' => 'Access your data for the teacher interface',
            'registrar' => 'Access your data for the registrar interface'
        ]);

        // Override authorization route to disable authorization prompt
        \Route::get('oauth/authorize', '\App\Http\Controllers\CustomAuthorizationController@authorize')->middleware(['web', 'auth']); 
    }
}
