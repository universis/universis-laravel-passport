<?php

namespace App\Http\Middleware;

use Closure;

class IpAccessControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedips = config('app.allowed_ips');
        if ($allowedips) {
            $ipArray = explode(",", $allowedips);
            if (!in_array($request->ip(), $ipArray, true)) {
                abort(403, "Forbidden");
            }
        }

        return $next($request);
    }
}
