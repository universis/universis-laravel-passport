<?php

namespace App\Http\Middleware;

use Closure;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('applocale') && array_key_exists(session('applocale'), config('languages'))) {
            app()->setLocale(session('applocale'));
        }
        return $next($request);
    }
}
