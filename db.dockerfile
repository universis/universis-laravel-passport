FROM mysql:5.7

ENV TZ=Europe/Athens

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone